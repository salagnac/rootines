#include "MultiGraph.h"

///Standard includes
#include <iostream>

///Own Includes
#include "PimpMyRoot.h"

using namespace std;

int MultiGraph::ClassCounter = 0;

MultiGraph::MultiGraph(TString xTitle, TString yTitle, TString title)
{
	mID = ClassCounter;
    ClassCounter++;
	
	mTitle = title;
	mXTitle = xTitle;
	mYTitle = yTitle;
	
	mHistoAxes = 0;
    mLegend = 0;

    mPimped = true;
}

MultiGraph::~MultiGraph()
{
    //if(mHistoAxes) delete mHistoAxes; ///deleted by TCanvas::Close()
}

int MultiGraph::Add(TGraph* graph, TString legendLabel, TString drawOption, TString legendOption)
{
    mGraph.push_back(graph);
	mLegendLabel.push_back(legendLabel);
	mDrawOption.push_back(drawOption);
    mLegendOption.push_back(legendOption);
    return mGraph.size() - 1;
}

void MultiGraph::Draw(TString drawOption)
{
	double xMin, xMax, yMin, yMax;
    for(int iGr = 0; iGr < mGraph.size(); iGr++)
	{
		TGraph* currGr = mGraph[iGr];
		double* x = currGr->GetX();
		double* y = currGr->GetY();
		for(int iPt = 0; iPt < currGr->GetN(); iPt++)
		{
			if(iGr == 0 && iPt == 0)
			{
				xMin = x[0];
				xMax = x[0];
				yMin = y[0];
				yMax = y[0];
			}
			
			if(xMin > x[iPt]) xMin = x[iPt];
			if(xMax < x[iPt]) xMax = x[iPt];
			if(yMin > y[iPt]) yMin = y[iPt];
			if(yMin < y[iPt]) yMax = y[iPt];
		}
	}
	
	double xOffset = (xMax - xMin) * 0.05;
	double yOffset = (yMax - yMin) * 0.05;
	
	xMin -= xOffset;
	xMax += xOffset;
	yMin -= yOffset;
	yMax += yOffset;
	
	if(mHistoAxes) delete mHistoAxes;
	mHistoAxes = new TH2F(Form("hMultiGraph_%d", mID), Form("%s; %s; %s", mTitle.Data(), mXTitle.Data(), mYTitle.Data()), 1000, xMin, xMax, 1000, yMin, yMax);

    if(mPimped)
    {
        mPimper.Pimp(mHistoAxes);
        for(int iGr = 0; iGr < mGraph.size(); iGr++)
        {
            mPimper.Pimp(mGraph[iGr]);
        }
    }

    mHistoAxes->Draw();
    for(int iGr = 0; iGr < mGraph.size(); iGr++)
	{
        TString opt = drawOption;
        if(opt == "") opt = mDrawOption[iGr];
        opt += "same";
        mGraph[iGr]->Draw(opt);
	}
}

void MultiGraph::DrawLegend(TString drawEntryOption)
{
    mLegend = new TLegend;
    for(int iGr = 0; iGr < mGraph.size(); iGr++)
    {
        TString opt = drawEntryOption;
        if(opt == "")
        {
            opt = mLegendOption[iGr];
        }
        mLegend->AddEntry(mGraph[iGr], mLegendLabel[iGr].Data(), opt.Data());
    }

    mPimper.Pimp(mLegend);
    mLegend->Draw();
}

void MultiGraph::SetPimped(bool pimped)
{
    mPimped = pimped;
}

void MultiGraph::ShowGraph()
{
    PimpMyRoot::ShowGraph();
}
