#include "Gnuplot.h"

#include "TROOT.h"
#include "TColor.h"
#include "FileTools.h"

#include <iostream>

using namespace std;

int Gnuplot::sm_ID = 0;

Gnuplot::Gnuplot(TString outputName, TString outputType)
{
    m_ID = sm_ID;
    sm_ID++;

    m_nbData = 0;

    m_xTitle = "";
    m_yTitle = "";

    TString nameScript = Form("plot%d.gnu", m_ID);

    m_fileScript.open(nameScript.Data(), ios::out | ios::trunc);


    m_fileScript << Form("set terminal %s enhanced size 20.02cm,13.55cm font \"Helvetica,20\"", outputType.Data()) << endl;
    m_fileScript << Form("set output \'%s.%s\'", outputName.Data(), outputType.Data()) << endl;

    m_fileScript << "set xrange [-10: 10]" << endl;
    m_fileScript << "set yrange [:]" << endl;

    m_fileScript << "set xtics 5" << endl;
    m_fileScript << "set ytics 10000" << endl;
    m_fileScript << "set mxtics 5" << endl;
    m_fileScript << "set mytics 5" << endl;
    m_fileScript << "set tics in nomirror" << endl;

    //m_fileScript << "set logscale y" << endl;

    m_fileScript << "set key reverse bmargin center" << endl;

    m_fileScript << "set style fill solid 0." << endl;
    m_fileScript << "set style line 1 linewidth 2" << endl;
}

Gnuplot::~Gnuplot()
{
    m_fileScript.close();

    TString cmdRmScript = Form("\\rm plot%d.gnu", m_ID);
    std::system(cmdRmScript.Data());

    for(int iData = 0; iData < m_nbData; iData++)
    {
        TString cmdRmData = Form("\\rm data%d_%d.dat", m_ID, iData);
        std::system(cmdRmData.Data());
    }
}

void Gnuplot::SetAxisTitle(TString xTitle, TString yTitle)
{
    m_xTitle = xTitle;
    m_yTitle = yTitle;
}

void Gnuplot::Add(TH1* histo, TString option)
{
    TColor* col = gROOT->GetColor(histo->GetLineColor());
    TString color = col->AsHexString();

    SaveAsASCII(histo, Form("data%d_%d.dat", m_ID, m_nbData));

    if(m_xTitle == "" || m_yTitle == "")
    {
        m_xTitle = histo->GetXaxis()->GetTitle();
        m_yTitle = histo->GetYaxis()->GetTitle();
    }

    TString mode;
    if(option.Contains("e"))
    {
        m_fileScript << "set bars small" << endl;
        mode = "(($2-$1)/2.0 + $1):3:4:5 with xyerrorbars pointtype 7 pointsize 0 linewidth 3";
    }
    else
    {
        mode = "(($2-$1)/2.0 + $1):3 with histeps linewidth 3";
    }

    TString property = Form("\'data%d_%d.dat\' using %s linecolor rgbcolor \'%s\' title \'test\'", m_ID, m_nbData, mode.Data(), color.Data());
    m_properties.push_back(property);

    m_nbData++;
}

void Gnuplot::Plot()
{
    m_fileScript << "set xlabel \"" << m_xTitle << "\" font \"Arial - Bold\"" << endl;
    m_fileScript << "set ylabel \"" << m_yTitle << "\" font \"Arial - Bold\"" << endl;

    m_fileScript << "plot ";
    for(int iData = 0; iData < m_nbData; iData++)
    {
        m_fileScript << m_properties[iData];
        if(iData != m_nbData - 1) m_fileScript << ", \\";
        m_fileScript << endl;
    }

    TString cmdPlot = Form("gnuplot plot%d.gnu", m_ID);
    std::system(cmdPlot.Data());
}
