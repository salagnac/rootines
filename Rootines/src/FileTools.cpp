#include "FileTools.h"

#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <TROOT.h>
#include <TSystemDirectory.h>
#include <TColor.h>
#include <TRegexp.h>

using namespace std;

Int_t FindFiles(TString** fileNames, TString name)
{
    if(fileNames == 0)
    {
        cout << endl << "FindFiles: Attention le pointeur de tableau en entree est nul !" << endl << endl;
        return -1;
    }

    TString fichiers[1000];

    TString dirname = name(0, name.Last('/'));
    name.Remove(0, name.Last('/') + 1);
    if(dirname == "") dirname = ".";
    TSystemDirectory dir(dirname, dirname);
    TList *files = dir.GetListOfFiles();
    Int_t N = 0;
    if(files)
    {
        TRegexp regexp(name);

        TSystemFile *file;
        TString fname, fnameMid;
        TIter next(files);
        while(file = (TSystemFile*) next())
        {
            if(!file->IsDirectory())
            {
                fname = file->GetName();
                Ssiz_t len;
                Ssiz_t pos = regexp.Index(fname, &len, 0);
                //cout << name << "\t" << fname << "\t" << len << " \t" << pos << endl;
                bool match = (len == fname.Length());

                if(match)
                {
                    cout << "File " << N+1 << ": " << fname.Data() << endl;
                    fichiers[N] = fname;
                    N++;
                }
            }
        }
    }

    *fileNames = new TString[N];
    TString* f = *fileNames;
    for(Int_t i = 0; i < N; i++)
    {
        f[i] = dirname + "/" + fichiers[i];
    }
    return N;
}

void PrintGraph(TGraph* graph, TString graphName)
{
	cout << "TGraphError: " + graphName << endl;

	cout << Form("Double_t X[%d] = {", graph->GetN());
	for(int i = 0; i < graph->GetN(); i++)
	{
		cout << graph->GetX()[i];
		if(i != graph->GetN() - 1) cout << ", ";
	}
	cout << "};" << endl;

	cout << Form("Double_t Y[%d] = {", graph->GetN());
	for(int i = 0; i < graph->GetN(); i++)
	{
		cout << graph->GetY()[i];
		if(i != graph->GetN() - 1) cout << ", ";
	}
	cout << "};" << endl;
}

void PrintGraph(TGraphErrors* graph, TString graphName)
{
	cout << "TGraphError: " + graphName << endl;

	cout << Form("Double_t X[%d] = {", graph->GetN());
	for(int i = 0; i < graph->GetN(); i++)
	{
		cout << graph->GetX()[i];
		if(i != graph->GetN() - 1) cout << ", ";
	}
	cout << "};" << endl;

	cout << Form("Double_t Y[%d] = {", graph->GetN());
	for(int i = 0; i < graph->GetN(); i++)
	{
		cout << graph->GetY()[i];
		if(i != graph->GetN() - 1) cout << ", ";
	}
	cout << "};" << endl;

	cout << Form("Double_t ErrX[%d] = {", graph->GetN());
	for(int i = 0; i < graph->GetN(); i++)
	{
		cout << graph->GetEX()[i];
		if(i != graph->GetN() - 1) cout << ", ";
	}
	cout << "};" << endl;

	cout << Form("Double_t ErrY[%d] = {", graph->GetN());
	for(int i = 0; i < graph->GetN(); i++)
	{
		cout << graph->GetEY()[i];
		if(i != graph->GetN() - 1) cout << ", ";
	}
	cout << "};" << endl;
}

void SaveAsASCII(TGraphErrors* graph, TString filename)
{
	ofstream fichier(filename.Data(), ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert

	if(fichier)
	{
		fichier << "X Y X_err Y_err" << endl;

		for(int i = 0; i < graph->GetN(); i++)
		{
			fichier << graph->GetX()[i] << " ";

			fichier << graph->GetY()[i] << " ";

			fichier << graph->GetEX()[i] << " ";

			fichier << graph->GetEY()[i] << endl;
		}
	}

	fichier.close();
}

void SaveAsASCII(TH1* histo, TString filename)
{
	ofstream fichier(filename.Data(), ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert

	if(fichier)
	{
		fichier << "X_low X_width Y X_err Y_err" << endl;

		for(int i = 0; i < histo->GetNbinsX(); i++)
		{
			fichier << histo->GetBinLowEdge(i) << " ";

			fichier << histo->GetBinLowEdge(i) + histo->GetBinWidth(i) << " ";

			fichier << histo->GetBinContent(i) << " ";

            fichier << histo->GetBinWidth(i) / 2. << " ";

			fichier << histo->GetBinError(i) << endl;
		}
	}

	fichier.close();
}

//void Gnuplot(TH1* histo)
//{
//	TColor* col = gROOT->GetColor(histo->GetLineColor());
//	TString color = Form("rgb(%.0f, %.0f, %.0f)", col->GetRed() * 255, col->GetGreen() * 255, col->GetBlue() * 255);
//	cout << color << endl;

//	SaveAsASCII(histo, "data.dat");

//	ofstream fichier("plot.gnu", ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert

//	fichier << "reset" << endl;

//	fichier << "set terminal pdf" << endl;
//	fichier << "set output \'out.pdf\'" << endl;

//	fichier << "set xrange [:]" << endl;
//	fichier << "set yrange [:]" << endl;

//	fichier << "set tics in nomirror" << endl;
//	fichier << "set xlabel \"" << histo->GetXaxis()->GetTitle() << "\"" << endl;
//	fichier << "set ylabel \"" << histo->GetYaxis()->GetTitle() << "\"" << endl;

//	//fichier << "set logscale y" << endl;

//	fichier << "set key reverse bmargin center" << endl;

//	fichier << "set style fill solid 0." << endl;
//	fichier << "set style line 1 linecolor " << color << " linewidth 2" << endl;

//	fichier << "plot \"data.dat\" using (($2-$1)/2.0 + $1):3 with histeps linestyle 1 title \"test\"" << endl;
//	fichier.close();

//  	std::system("gnuplot plot.gnu");
//  	std::system("\\rm plot.gnu");
//	std::system("\\rm data.dat");
//}

