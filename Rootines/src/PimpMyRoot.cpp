#include "PimpMyRoot.h"

///Standard includes
#include <iostream>

///ROOT includes
#include "TStyle.h"
#include "TColor.h"
#include "TCanvas.h"
#include "TList.h"
#include "TLatex.h"
#include "TLegendEntry.h"
#include "TF1.h"
#include "TFrame.h"

///Other includes
#include "MultiGraph.h"


using namespace std;

void SetStyle()
{
    gStyle->SetOptStat("");
    gStyle->SetOptFit(0111);
    gStyle->SetTitleFont(62, "xy");
    gStyle->SetTitleFont(62, "uu");
    gStyle->SetStatFont(62);
    /*gStyle->SetStatX(0.1);
    gStyle->SetStatY(0.1);*/
    gStyle->SetHistLineWidth(2);
    gStyle->SetTitleYOffset(1.2);
    gStyle->SetLegendFont(62);
    gStyle->SetLineWidth(1);
    gStyle->SetMarkerSize(1.5);
    gStyle->SetMarkerStyle(23);
    gStyle->SetOptLogz();

    const Int_t NRGBs = 5;
    const Int_t NCont = 255;

    Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
}

ColorPalette::ColorPalette()
{
    mColor.push_back(kAzure-2);
    mColor.push_back(kPink);
    mColor.push_back(kTeal-1);
    mColor.push_back(kOrange-4);
    mColor.push_back(kViolet+1);
    mColor.push_back(kGray+1);
    mColor.push_back(kMagenta-9);
    mColor.push_back(kOrange-3);

    mCurrentIndex = 0;
}

Color_t ColorPalette::GetColor(int colorID)
{
    Color_t col;
    if(colorID == -1)
    {
        col = mCurrentIndex;
        mCurrentIndex++;
    }
    else
    {
        col = colorID;
    }

    return mColor[col %  mColor.size()];
}

unsigned int ColorPalette::GetNbColor()
{
    return mColor.size();
}

void ColorPalette::ShowPalette()
{
    ColorPalette palette;

    TCanvas* can = new TCanvas("canPalette", "Palette Couleur", 600, 100);

    int N = palette.GetNbColor();
    for(int i = 0; i < N; i++)
    {
        TBox* box = new TBox(i * 1. / N, 0, (i + 1) * 1. / N, 1);
        box->SetFillColor(palette.GetColor(i));
        box->Draw();
    }
    can->Update();
}

PimpMyRoot::PimpMyRoot()
{
    mTextFontStyle = 62;
    mTextSize = 0.05;

    ///Graph and Histo
    mAxisLabelFont = 62;
    mAxisLabelSize = 0.038;
    mAxisLabelOffsetX = 0.010;
    mAxisLabelOffsetY = 0.015;
    mAxisLabelOffsetZ = 0.010;
    mAxisTitleFont = 62;
    mAxisTitleSize = 0.05;
    mAxisTitleOffsetX = 1.1;
    mAxisTitleOffsetY = 1.15;
    mAxisTitleOffsetZ = 1.1;

    ///Histo
    mHistoLineWidth = 1;
    mHistoMarkerStyle = kFullTriangleDown;
    mHistoMarkerSize = 1;

    ///Graph
    mGraphLineWidth = 1;
    mGraphMarkerStyle = kFullCircle;
    mGraphMarkerSize = 0.7;

    ///Legend
    mLegendX = 0.9;
    mLegendY = 0.8;
    mLegendMotifSize = 0.05;
    mLegendTextFont = 43;
    mLegendTextSize = 18;
    mLegendBorderSize = 0;

    ///Canvas
    mCanWindowWidth = 680;
    mCanWindowHeight = 450;
    mCanLeftMargin = 0.12;
    mCanRightMargin = 0.04;
    mCanTopMargin = 0.04;
    mCanBottomMargin = 0.12;
}

void PimpMyRoot::Pimp(TH1* histo, TString option, TString title, TString titleX, TString titleY, TString titleZ, Color_t color)
{
    histo->SetStats(false);

    histo->SetTitleFont(mAxisTitleFont, "X");
    histo->SetTitleFont(mAxisTitleFont, "Y");
    histo->SetTitleFont(mAxisTitleFont, "Z");

    histo->SetTitleOffset(mAxisTitleOffsetX, "X");
    histo->SetTitleOffset(mAxisTitleOffsetY, "Y");
    histo->SetTitleOffset(mAxisTitleOffsetZ, "Z");

    histo->SetTitleSize(mAxisTitleSize, "X");
    histo->SetTitleSize(mAxisTitleSize, "Y");
    histo->SetTitleSize(mAxisTitleSize, "Z");

    histo->GetXaxis()->SetLabelSize(mAxisLabelSize);
    histo->GetYaxis()->SetLabelSize(mAxisLabelSize);
    histo->GetZaxis()->SetLabelSize(mAxisLabelSize);
    histo->GetXaxis()->SetLabelOffset(mAxisLabelOffsetX);
    histo->GetYaxis()->SetLabelOffset(mAxisLabelOffsetY);
    histo->GetZaxis()->SetLabelOffset(mAxisLabelOffsetX);

    //histo->GetXaxis()->CenterTitle();
    //histo->GetYaxis()->CenterTitle();

    if(title != "") histo->SetTitle(title);
    if(titleX != "") histo->SetXTitle(titleX);
    if(titleY != "") histo->SetYTitle(titleY);
    if(titleZ != "") histo->SetZTitle(titleZ);

    if(title == "notitle") histo->SetTitle("");
    if(titleX == "notitle") histo->SetXTitle("");
    if(titleY == "notitle") histo->SetYTitle("");
    if(titleZ == "notitle") histo->SetZTitle("");

    if(color == -1) color = mColorPalette.GetColor();
    histo->SetLineColor(color);
    histo->SetLineWidth(mHistoLineWidth);

    if(option.Contains("p"))
    {
        histo->SetMarkerStyle(mHistoMarkerStyle);
        histo->SetMarkerSize(mHistoMarkerSize);
        histo->SetMarkerColor(color);
    }
}

void PimpMyRoot::Pimp(TGraph* graph, TString title, TString titleX, TString titleY, TString titleZ, Color_t color)
{
    graph->SetTitle(title);

    Pimp(graph->GetHistogram(), "", "", titleX, titleY, titleZ, 0);

    if(color == -1) color = mColorPalette.GetColor();

    graph->SetFillColor(color);

    graph->SetLineWidth(mGraphLineWidth);
    graph->SetLineColor(color);

    graph->SetMarkerColor(color);
    graph->SetMarkerStyle(mGraphMarkerStyle);
    graph->SetMarkerSize(mGraphMarkerSize);
}

void PimpMyRoot::Pimp(TLegend* legend)
{
    Int_t height = legend->GetNRows();

    TList* entries = legend->GetListOfPrimitives();
    Int_t nEntry = entries->GetSize();

    TVirtualPad* currCan = gPad;

    TCanvas can("cL","", 680, 480);
    can.Draw();
    Double_t maxLength = 0;
    for(Int_t iEntry = 0; iEntry< nEntry; iEntry++)
    {
        TString str = ((TLegendEntry*) entries->At(iEntry))->GetLabel();
        TText t(0, 0, str);
        t.SetTextFont(43);
        t.SetTextSize(18);
        t.Draw();
        can.Modified();
        can.Update();
        Double_t lg = t.GetBBox().fWidth / 680.;
        //cout << str << " " << t.GetBBox().fWidth << endl;
        if(maxLength < lg) maxLength = lg;
    }
    can.Close();
    currCan->cd();


    double lengthPicture = 0.05;

    legend->SetX1NDC(0.899425 - lengthPicture - maxLength - 0.01);
    legend->SetY1NDC(0.900424 - 0.06 * height);
    legend->SetX2NDC(0.899425);
    legend->SetY2NDC(0.900424);

    legend->SetMargin(lengthPicture / (legend->GetX2NDC()-legend->GetX1NDC()));
    legend->SetTextFont(43);
    legend->SetTextSize(18);
    legend->SetBorderSize(0);
    legend->SetFillStyle(0);
}

void PimpMyRoot::Pimp(TCanvas* can)
{
    can->SetWindowSize(680, 450);
    can->SetLeftMargin(0.12);
    can->SetRightMargin(0.04);
    can->SetTopMargin(0.04);
    can->SetBottomMargin(0.12);
}

void PimpMyRoot::ShowHisto()
{
    PimpMyRoot pimper;

    TCanvas* chist = new TCanvas("chist", "Histogram");
    pimper.Pimp(chist);

    TH1D* h1d_1 = new TH1D("h1d_1", "Histo TH1D", 100, -10000, 10000);

    pimper.Pimp(h1d_1, "", "notitle", "Test title X", "Test title Y");
    TF1* f1 = new TF1("f1", "exp(-x**2/(2*1500**2))", -100, 100);
    h1d_1->FillRandom("f1", 1000000);
    h1d_1->Draw();

    TH1D* h1d_2 = new TH1D("h1d_2", "Histo TH1D", 100, -10000, 10000);
    h1d_2->Sumw2();

    pimper.Pimp(h1d_2, "P", "notitle", "Test title X", "Test title Y");

    TF1* f2 = new TF1("f2", "exp(-(x-2000)**2/(2*1500**2))", -100, 100);
    h1d_2->FillRandom("f2", 1000000);
    h1d_2->Draw("same");

    TLegend* legend = new TLegend;
    legend->AddEntry(h1d_1, "Long name histo 1", "l");
    legend->AddEntry(h1d_2, "LONG NAME HISTO 2", "pl");
    pimper.Pimp(legend);
    legend->Draw();

    chist->Update();
}

void PimpMyRoot::ShowGraph()
{
    static int compt = 0;

    MultiGraph mg("Title X", "Title Y");

    const int N = 20;
    for(int iGr = 0; iGr < 5; iGr++)
    {
        double x[N];
        double y[N];
        for(int iPt = 0; iPt < N; iPt++)
        {
            x[iPt] = iPt;
            y[iPt] = (2 * pow(x[iPt], 2) - 3 * x[iPt] + 3) * pow(2, iGr);
        }
        TGraph* gr = new TGraph(N, x, y);
        mg.Add(gr, Form("Graph %d",iGr), "PL", "PL");
    }

    TCanvas* canGr = new TCanvas(Form("canGr%d", compt), "MultiGraph");

    PimpMyRoot pimper;
    pimper.Pimp(canGr);

    mg.Draw("PL");
    mg.DrawLegend();

    canGr->Update();

    compt++;
}
