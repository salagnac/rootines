#ifndef FILETOOLS_H
#define FILETOOLS_H

#endif // FILETOOLS_H

#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1.h>

Int_t FindFiles(TString** fileNames, TString name = "");

void PrintGraph(TGraph* graph, TString graphName = "");

void PrintGraph(TGraphErrors* graph, TString graphName = "");

void SaveAsASCII(TGraphErrors* graph, TString filename);

void SaveAsASCII(TH1* histo, TString filename);

//void Gnuplot(TH1* histo);

