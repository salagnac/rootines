#ifndef PimpMyRoot_h
#define PimpMyRoot_h

///Standard includes
#include <vector>

///ROOT includes

#include "Rtypes.h"
#include "TH1.h"
#include "TGraph.h"
#include "TLegend.h"
#include "TString.h"
#include "TCanvas.h"

class ColorPalette
{
    public:
        ColorPalette();
        Color_t GetColor(int colorID = -1);
        unsigned int GetNbColor();
        void SetIndex(unsigned int colorID);
        static void ShowPalette();

    private:
        int mCurrentIndex;
        std::vector<Color_t> mColor;
};

void SetStyle();

class PimpMyRoot
{
    public:
        PimpMyRoot();
        void Pimp(TH1* histo, TString option = "", TString title = "", TString titleX = "", TString titleY = "", TString titleZ = "", Color_t color = -1);
        void Pimp(TGraph* graph, TString title = "", TString titleX = "", TString titleY = "", TString titleZ = "", Color_t color = -1);
        void Pimp(TLegend* legend);
        void Pimp(TCanvas* can);
        static void ShowGraph();
        static void ShowHisto();

    private:
        ColorPalette mColorPalette;
        int mTextFontStyle;
        double mTextSize;

        ///Graph and Histo
        int mAxisLabelFont;
        double mAxisLabelSize;
        double mAxisLabelOffsetX;
        double mAxisLabelOffsetY;
        double mAxisLabelOffsetZ;
        int mAxisTitleFont;
        double mAxisTitleSize;
        double mAxisTitleOffsetX;
        double mAxisTitleOffsetY;
        double mAxisTitleOffsetZ;

        ///Histo
        double mHistoLineWidth;
        EMarkerStyle mHistoMarkerStyle;
        double mHistoMarkerSize;

        ///Graph
        double mGraphLineWidth;
        EMarkerStyle mGraphMarkerStyle ;
        double mGraphMarkerSize;

        ///Legend
        double mLegendX;
        double mLegendY;
        double mLegendMotifSize;
        int mLegendTextFont;
        double mLegendTextSize;
        double mLegendBorderSize;

        ///Canvas
        int mCanWindowWidth;
        int mCanWindowHeight;
        double mCanLeftMargin;
        double mCanRightMargin;
        double mCanTopMargin;
        double mCanBottomMargin;
};



#endif
