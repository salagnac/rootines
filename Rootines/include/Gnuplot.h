#ifndef GNUPLOT_H
#define GNUPLOT_H

#include <fstream>
#include "TH1.h"
#include "TString.h"
#include <vector>

class Gnuplot
{
    public:
        Gnuplot(TString outputName = "plot", TString outputType = "pdf");
        ~Gnuplot();
        void SetAxisTitle(TString xTitle,TString yTitle);
        void Add(TH1* histo, TString option = "");
        void Plot();

    private:
        int m_ID;
        std::ofstream m_fileScript;
        int m_nbData;
        TString m_xTitle;
        TString m_yTitle;
        std::vector<TString> m_properties;
        double m_xMin;
        double m_xMax;

        static int sm_ID;


};

#endif // GNUPLOT_H
