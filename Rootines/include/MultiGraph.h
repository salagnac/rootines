#ifndef MULTIGRAPH_H
#define MULTIGRAPH_H

///ROOT includes
#include "Rtypes.h"
#include "TGraph.h"
#include "TH2F.h"

///STL includes
#include <vector>

///Own includes
#include "PimpMyRoot.h"

class MultiGraph
{
    public:
        MultiGraph(TString xTitle = "", TString yTitle = "", TString title = "");
        ~MultiGraph();
        int Add(TGraph* graph, TString legendLegend = "", TString drawOption = "", TString legendOption = "");
        void Draw(TString option = "");
        void DrawLegend(TString drawEntryOption = "");
        void SetPimped(bool pimped);
        static void ShowGraph();

    private:
        int mID;
        std::vector<TGraph*> mGraph;
        std::vector<TString> mDrawOption;
        std::vector<TString> mLegendLabel;
        std::vector<TString> mLegendOption;
        TH2F* mHistoAxes;
        TLegend* mLegend;
        TString mTitle;
        TString mXTitle;
        TString mYTitle;
        bool mPimped;
        PimpMyRoot mPimper;

        static int ClassCounter;
};

#endif
