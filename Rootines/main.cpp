///Standard includes
#include <iostream>
#include "signal.h"

///ROOT includes
#include "TApplication.h"
#include "TROOT.h"

///Own includes
#include "PimpMyRoot.h"


using namespace std;

void Interrupt(int arg)
{

  printf("Got a control-C, exiting\n");
  exit(0);

}

int main(int argc, char **argv)
{
    signal(SIGINT,Interrupt);

    argc=0;
    TApplication theApp("App",&argc,argv);

    PimpMyRoot::ShowHisto();
    PimpMyRoot::ShowGraph();

//    ColorPalette::ShowPalette();

    if(gROOT->GetListOfCanvases()->GetEntries() > 0)
    {
        printf("Done (exit root, or hit Control-C)\n");
        theApp.Run();
    }

    return 0;
}

